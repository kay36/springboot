package org.auth.apple.services;


import com.github.mikereem.apple.signin.model.User;
import com.github.mikereem.apple.signin.util.AppleSigninUtil;
import io.jsonwebtoken.Claims;
import org.auth.apple.AppleAuthApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.logging.Logger;

@Service
public class Auth_login
{

    private AppleSigninUtil appleSigning;

    @Value("${apple.auth.clientId}")
    private String clientId;


    @Value("${apple.auth.key.id}")
    private String Apple_Key_Id;

    @Value("${apple.auth.Team.id}")
    private String Apple_Team_Id;

    public void init() {

        appleSigning = new AppleSigninUtil();
        try {
            AppleSigninUtil.init(Apple_Key_Id, Apple_Team_Id, new InputStreamReader(
                    Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("AuthKey_LAXP3KSM5L.p8"))));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @PostConstruct
    public void testAuth(){
        init();
        authLogin("eyJraWQiOiJXNldjT0tCIiwiYWxnIjoiUlMyNTYifQ.eyJpc3MiOiJodHRwczovL2FwcGxlaWQuYXBwbGUuY29tIiwiYXVkIjoiY29tLnNoYW5rYXJJQVMudGVzdCIsImV4cCI6MTY1OTAxMDk3MSwiaWF0IjoxNjU4OTI0NTcxLCJzdWIiOiIwMDE0MTkuYjkwZGNjOTE2Njk0NGExOWIzZjYwYjliNTE0YWNhZDIuMjAyOSIsImNfaGFzaCI6IjBTSVVHbmEzOUw2YnNORUtibUpEdWciLCJlbWFpbCI6Im5hcmFzaGltaGFhLmthbm5hbkB2aW5ieXRlcy5jb20iLCJlbWFpbF92ZXJpZmllZCI6InRydWUiLCJhdXRoX3RpbWUiOjE2NTg5MjQ1NzEsIm5vbmNlX3N1cHBvcnRlZCI6dHJ1ZX0.CwFxCADbsjS7P54Phv07BIv1Btq2fnugZ6civEdKE73W6pNbJZGekTDjBFl9aibx9tQThtFOUY3SgInBEZHJvvAE4RG3U70Jem_aQzQLtTz2C9Y1UFo5oxcS3YgeSWJIDqF7MizSbhlJzgfUwuFlgei3dnRusIXnrAevxXyMDsYSmZrmxSutkE738yASG8XkcIchD2kf05AFwVCuLdvlfFaQU6EpVyVNNTvFifrTm9NvLWmY0eddhdlJX_Xp74gYgCycPLOoTN3qRDFtgWW7sQQjqhzVKXDVEz7NOIIOAtvkXBOx9pkMoIuBSF2Qd6-AQGTd5UCQcxCvINYD6K485w", "c1b2094767a604b6db2cd3648b3455e21.0.srurz.Y1E6lOJQYEXkx5I3Pb3d-Q");
    }

    public void auth(String user, String identityToken, String authorizationCode){
        Claims claims = null;
        try {
            claims = appleSigning.authorize(clientId, identityToken, authorizationCode, "https://www.shankariasacademy.com/");
            if (claims != null) {
                System.out.println("Apple authentication validated for user " + appleSigning.getEmail(claims));
                User userResponse = null;
                if (user != null) {
                    userResponse = appleSigning.parseUser(user);
                    System.out.println("User received:" + userResponse);
                }
            }
        } catch (Throwable t) {
            System.out.println("Error during apple authorization validation of mobile registration" + t);
        }
    }

    public void authLogin(String identityToken, String authorizationCode){

        try {
            System.out.println("kishor");
            Claims claims = AppleSigninUtil.authorize("com.shankarIAS.test", identityToken, authorizationCode, null);
            System.out.println(claims);
            if (claims != null) {
                System.out.println("Apple authentication validated for user " + appleSigning.getEmail(claims));
            }else {
                System.out.println(claims);
                System.out.println(claims);
            }
        } catch (Throwable t) {
            System.out.println(t);
        }
    }
}
